// MIT License

// Copyright (c) [2019] [Jamorn Sriwasansak and Rex West]

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

(function QuadCompare() {

  let Sprintf = function(str, args)
  {
    return str.replace(/{(\d+)}/g, function(match, number)
    {
      return typeof args[number] != 'undefined' ? args[number] : match;
    });
  };

  let AddTextLabel = function(parentElement, text)
  {
    let label = document.createElement('p');
    label.innerHTML = text;
    label.classList.add('quadcompare-label');
    parentElement.appendChild(label);
    return label;
  };

  let BootstrapStyle = function(name, definition)
  {
    let style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = Sprintf('.{0} { {1} }', [name, definition]);
    document.getElementsByTagName('head')[0].appendChild(style);
  };

  let MakeCssRect = function(top, right, bottom, left)
  {
    return Sprintf('rect({0}px {1}px {2}px {3}px)', [top, right, bottom, left]);
  };

  let RemoveTextElements = function(element)
  {
    let childs = element.childNodes;
    for (let iChild = 0; iChild < childs.length; iChild++)
    {
      let child = childs[iChild];
      if (child.nodeType == 3) element.removeChild(child);
    }
  };

  let SetElementWidthHeight = function(element, width, height)
  {
    let style = element.style;
    style.width = style.minWidth = style.maxWidth = (width + 'px');
    style.height = style.minHeight = style.maxHeight = (height + 'px');
  };

  let GetProperWidthHeight = function(quadCompElement)
  {
    let images = quadCompElement.children;
    let result = {width:0, height:0};

    // if not has width use maximum image width
    if (!quadCompElement.hasAttribute('width'))
    {
      for (let i = 0; i < images.length; i++)
      {
        result.width = Math.max(result.width, images[i].naturalWidth);
      }
    }
    else
    {
      result.width = parseInt(quadCompElement.getAttribute('width'), 10);
    }

    // if not has height use maximum image height
    if (!quadCompElement.hasAttribute('height'))
    {
      for (let i = 0; i < images.length; i++)
      {
        result.height = Math.max(result.height, images[i].naturalHeight);
      }
    }
    else
    {
      result.height = parseInt(quadCompElement.getAttribute('height'), 10);
    }

    return result;
  };

  let GetDoCross = function(quadCompElement)
  {
    if (!quadCompElement.hasAttribute('cross'))
    {
      return false;
    }
    return quadCompElement.getAttribute('cross') == "cross";
  };

  let SmartImageRepeat = function(quadCompElement)
  {
    let removeAltText = function(element) { element.alt = ""; return element; };
    let images = quadCompElement.children;
    if (images.length == 1)
    {
      quadCompElement.appendChild(removeAltText(images[0].cloneNode(true)));
      quadCompElement.appendChild(removeAltText(images[0].cloneNode(true)));
      quadCompElement.appendChild(removeAltText(images[0].cloneNode(true)));
    }
    if (images.length == 2)
    {
      quadCompElement.appendChild(removeAltText(images[0].cloneNode(true)));
      quadCompElement.appendChild(removeAltText(images[1].cloneNode(true)));
    }
    if (images.length == 3)
    {
      quadCompElement.appendChild(removeAltText(images[2].cloneNode(true)));
    }
  };

  let MakeCompare = function(quadCompElement)
  {
    //Clean up the newlines, etc. that appear between images
    RemoveTextElements(quadCompElement);

    //Make the comparison visible
    quadCompElement.classList.add('quadcompare-visible');

    //Get proper width and height for quadCompElement
    let numImages = quadCompElement.children.length;
    let imageSize = GetProperWidthHeight(quadCompElement);
    let width = imageSize.width;
    let height = imageSize.height;
    SetElementWidthHeight(quadCompElement, width, height);

    //Repeat images if num of images is not 4 in a smart way
    SmartImageRepeat(quadCompElement);

    //Set all images width and height
    let images = quadCompElement.children;
    let textLabels = [];
    for (let i = 0; i < 4; i++)
    {
      SetElementWidthHeight(images[i], width, height);
      images[i].classList.add('quadcompare-image');
      textLabels.push(AddTextLabel(quadCompElement, images[i].alt));
    }

    //Check if we need cross
    let cross = GetDoCross(quadCompElement);
    let splitTopLR = cross == true ? 1 : 0;
    let splitTopBottom = (cross == true && (numImages == 3 || numImages == 4)) ? 1 : 0;
    let splitBottomLR = (cross == true && (numImages == 2 || numImages == 4)) ? 1 : 0;

    //Set pos callback function
    let setMaskAndLabelPositions = function(x, y)
    {
      images[0].style.clip = MakeCssRect(0, x - splitTopLR, y, 0);
      images[1].style.clip = MakeCssRect(0, width, y, x);
      images[2].style.clip = MakeCssRect(y + splitTopBottom, x - splitBottomLR, height, 0);
      images[3].style.clip = MakeCssRect(y + splitTopBottom, width, height, x);

      textLabels[0].style.left = (x - textLabels[0].offsetWidth) + 'px';
      textLabels[0].style.top = (y - textLabels[0].offsetHeight) + 'px';
      textLabels[1].style.left = x + 'px';
      textLabels[1].style.top = (y - textLabels[1].offsetHeight) + 'px';
      textLabels[2].style.left = (x - textLabels[2].offsetWidth) + 'px';
      textLabels[2].style.top = y + 'px';
      textLabels[3].style.left = x + 'px';
      textLabels[3].style.top = y + 'px';
    };
  
    let moveHandler = function(event)
    {
      let rect = quadCompElement.getBoundingClientRect();
      let x = Math.max(rect.left, Math.min(event.clientX, rect.right)); 
      let y = Math.max(rect.top, Math.min(event.clientY, rect.bottom));
      setMaskAndLabelPositions(x - rect.left, y - rect.top);
    };
  
    //On mousemove inside of the comparison update the image mask position
    quadCompElement.addEventListener('mousemove', moveHandler);
    quadCompElement.addEventListener('touchstart', function(e)
    {
      moveHandler(e.touches[0]);
    });
    quadCompElement.addEventListener('touchmove', function(e)
    {
      moveHandler(e.touches[0]);
    });
  
    //Set the default position
    setMaskAndLabelPositions(width / 2, height / 2);
  };

  window.addEventListener('DOMContentLoaded', function()
  {
    setTimeout(function()
    {
      //Set up the default styles
      BootstrapStyle('quadcompare',
        'touch-action: none;' +
        'cursor: none;' +
        'visibility: hidden;' +
        'display: inline-block;' +
        'position: relative;' +
        'background-color: gray;' +
        'overflow: hidden;');

      BootstrapStyle('quadcompare-visible',
        'visibility: visible;');

      BootstrapStyle('quadcompare-label',
        'position: absolute;' +
        'margin: 0px;' +
        'padding: 7px;' +
        'color: white;' +
        'white-space: nowrap;' +
        'text-shadow: 0 0 1.5px black;');

      BootstrapStyle('quadcompare-image',
        'overflow: hidden;' +
        'display: inline-block;' +
        'background-color: black;' +
        'position: absolute;');

      //Configure an quadcompare elements in the document
      let quadCompElements = document.getElementsByClassName('quadcompare');
      for (let i = 0; i < quadCompElements.length; i++)
      {
        MakeCompare(quadCompElements[i]);
      }
    }, 0);
  });

})();
